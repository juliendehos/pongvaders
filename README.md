# pongvaders

Pong and Spaceinvaders in a single game, for
[Developpez.com WE-JV9](https://www.developpez.net/forums/d1991603/applications/developpement-2d-3d-jeux/neuvieme-week-end-programmation-jeux-video-developpez-com-se-tiendra-30-aout-1er-septembre/),
see the [associated thread](https://www.developpez.net/forums/d2001369/applications/developpement-2d-3d-jeux/projets/we-jv9-pongvaders-pong-p-spaceinvaders/).

[Play online !](https://juliendehos.gitlab.io/pongvaders)

Implemented using [Haskell](https://www.haskell.org/), 
[Miso](https://github.com/dmjio/miso),
[Nix](https://nixos.org/) and
[Gitlab](https://gitlab.com/).
See also [miso-invaders](https://gitlab.com/juliendehos/miso-invaders).

How to build:

```
nix-shell --run make
firefox public/index.html
```

![](pongvaders.mp4)

