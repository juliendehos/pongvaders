module Game where

import qualified Constants as C
import qualified Bounce as B
import qualified Data.Map as M
import qualified Point as P
import Item

data Status = Welcome | Running | Won | Lost deriving (Eq)

data Game = Game
    { _status :: Status
    , _inputUp :: Bool
    , _inputDown :: Bool
    , _inputLeft :: Bool
    , _inputRight :: Bool
    , _inputFire :: Bool
    , _rands :: [Double]
    , _firetime :: Double
    , _pongBall :: Item
    , _pongPaddle :: Item
    , _patrick :: Item
    , _bob :: Item
    , _bullets :: [Item]
    , _invaders :: [Item]
    } deriving (Eq)

doCycle :: Int -> [a] -> [a]
doCycle n xs = let (xs0, xs1) = splitAt n xs in xs1++xs0

getCycle :: Int -> [a] -> ([a], [a])
getCycle n xs = let (xs0, xs1) = splitAt n xs in (xs0, xs1++xs0)

createGame :: Bool -> [Double] -> Game
createGame isRunning rands0 = 
    Game status False False False False False rands1 0 pongBall pongPaddle pat bob [] myInvaders
    where status = if isRunning then Running else Welcome
          pongBall = Item (C.pongBallD, C.pongBallD)
                          (C.gameWidthD/2, C.gameHeightD/2)
                          ((-200) * cos ballAngle, 200 * sin ballAngle)
          pongPaddle = Item (C.pongPaddleWD, C.pongPaddleHD)
                            (C.pongPaddleWD, C.gameHeightD/2)
                            (0, 0)
          pat = Item (C.patWidthD, C.patHeightD)
                     (C.gameWidthD - C.patWidthD, C.gameHeightD/2)
                     (0, 0)
          bob = Item (C.bobWidthD, C.bobHeightD) 
                     ( C.gameWidthInvadersD/2 + C.gameXmarginD
                     , C.gameHeightInvadersD - C.bobHeightD + C.gameYmarginD)
                     (0, 0)
          ([mag, dir, ballDir], rands1) = getCycle 3 rands0
          ballAngle = (ballDir - 0.5) * pi * 0.75
          vx = (100 + 100 * mag) * (if dir < 0.5 then 1 else -1)
          myInvaders = [ Item (70, 20) 
                              ( fromIntegral x * 100 + C.gameWidthInvadersD/2
                              , fromIntegral y * 50 + 20 + C.gameYmarginD)
                              (vx, 0)
                         | x<-[-1..(1::Int)], y<-[0..(2::Int)] ]

updatePongBall :: Double -> Game -> Game
updatePongBall time g = g { _pongBall = ball2, _status = status }
    where ball0@(Item _ pos vel@(vx, vy)) = _pongBall g
          (x1, y1) = pos P.+ time P.* vel
          (yy1, vy1)
            | y1 > C.gameHeightD - C.pongBallD = (C.gameHeightD - C.pongBallD, - vy)
            | y1 < C.pongBallD = (C.pongBallD, - vy)
            | otherwise = (y1, vy)
          ball1 = ball0 { _pos = (x1, yy1), _vel = (vx, vy1) }
          ball2 = B.bounceItems ball0 ball1 $ [_pongPaddle g, _patrick g, _bob g] ++ _invaders g
          status = if fst (_pos ball2) > C.gameWidthD then Lost else _status g

updatePongPaddle :: Double -> Game -> Game
updatePongPaddle _ g = g { _pongPaddle = paddle1 }
    where paddle0 = _pongPaddle g
          paddleX0 = fst $ _pos paddle0
          paddleY1 = snd $ _pos $ _pongBall g
          paddle1 = paddle0 { _pos = (paddleX0, paddleY1) }

updatePatrick :: Double -> Game -> Game
updatePatrick time g = g { _patrick = pat1 }
    where dy = time * 300
          pat0 = _patrick g
          (xPat, yPat) = _pos pat0
          dd = if _inputDown g then dy else 0
          du = if _inputUp g then -dy else 0
          y1 = max 0 $ min C.gameHeightD $ yPat + dd + du
          pat1 = pat0 { _pos = (xPat, y1) }

updateBob :: Double -> Game -> Game
updateBob time g = fireBobBullet time g1
    where dx = time * 300
          dl = if _inputLeft g then -dx else 0
          dr = if _inputRight g then dx else 0
          bob0 = _bob g
          (x0, y) = _pos bob0
          minx = C.gameXmarginD
          maxx = C.gameWidthInvadersD + C.gameXmarginD
          x1 = max minx $ min maxx $ dl + dr + x0
          bob1 = bob0 { _pos = (x1, y) }
          g1 = g { _bob = bob1 }

fireBobBullet :: Double -> Game -> Game
fireBobBullet time g = if canFire then mFire else mNofire
    where canFire = _inputFire g && _firetime g > C.timeCanFire
          (x, y) = _pos $ _bob g
          bullet = Item (3, 9) (x, y-40) (0, -200)
          mFire = g { _bullets = bullet : _bullets g, _firetime = 0 }
          mNofire = g { _firetime = time + _firetime g }

updateInvaders :: Double -> Game -> Game
updateInvaders time g = if null myInvaders then g else g3
    where myInvaders = _invaders g
          i1 = map (autoUpdateItem time) myInvaders
          xs = map (fst . _pos) myInvaders
          x1min = minimum xs
          x1max = maximum xs
          v@(vx, _) = _vel $ head myInvaders
          move v0 v1 i = i { _pos = _pos i P.+ time P.* v0, _vel = v1 }
          maxx = C.gameWidthInvadersD + C.gameXmarginD
          minx = C.gameXmarginD
          i2 | vx>0 && x1max>maxx = map (move (maxx-x1max, 0) (P.negate v)) i1
             | vx<0 && x1min<minx = map (move (minx-x1min, 0) (P.negate v)) i1
             | otherwise = i1
          g2 = g { _invaders = i2 }
          g3 = fireInvadersBullets g2

fireInvadersBullets :: Game -> Game
fireInvadersBullets g = g { _bullets = _bullets g ++ bs, _rands = rands3 }
    where invadersPos = map _pos $ _invaders g
          fInsert pMap (x,y) = M.insertWith max x y pMap
          fighters0 = M.toList $ foldl fInsert M.empty invadersPos
          (rands0, rands1) = getCycle (length fighters0) (_rands g)
          (rands2, rands3) = getCycle (length fighters0) rands1
          fighters1 = [ (p, v) | (p, r, v) <- zip3 fighters0 rands0 rands2, r > 0.99 ]
          createBullet ((x, y), v) = Item (3, 9) (x, y+20) (0, 200+v*100)
          bs = map createBullet fighters1

autoUpdateItem :: Double -> Item -> Item
autoUpdateItem t i@(Item _ pos vel) = i { _pos = pos P.+ t P.* vel }

updateBullets :: Double -> Game -> Game
updateBullets time g = g { _bullets = b2 }
    where b1 = map (autoUpdateItem time) (_bullets g)
          b2 = filter (\b -> snd (_pos b) < C.gameHeightD && snd (_pos b) > 0) b1

updateCollisions :: Game -> Game
updateCollisions g = g1 { _invaders = i1, _bullets = b2, _status = st }
    where (b1, i1) = runCollisions (_bullets g) (_invaders g)
          (b2, p2) = runCollisions b1 [_bob g]
          st | null i1 = Won 
             | null p2 = Lost
             | otherwise = Running
          g1 = if st == Running then g 
               else g { _inputLeft = False, _inputRight = False, _inputFire = False }

testCollision :: Item -> Item -> Bool
testCollision (Item as ap _) (Item bs bp _) =
    ((bx0 < ax0 && ax0 < bx1) || (bx0 < ax1 && ax1 < bx1)) &&
    ((by0 < ay0 && ay0 < by1) || (by0 < ay1 && ay1 < by1))
    where (ax0, ay0) = ap P.- 0.5 P.* as
          (ax1, ay1) = ap P.+ 0.5 P.* as
          (bx0, by0) = bp P.- 0.5 P.* bs
          (bx1, by1) = bp P.+ 0.5 P.* bs

runCollisions :: [Item] -> [Item] -> ([Item], [Item])
runCollisions [] is = ([], is)
runCollisions (b:bs) is = (bs1++bs2, is2)
    where is1 = filter (not . testCollision b) is
          bs1 = [b | length is1 == length is]
          (bs2, is2) = runCollisions bs is1

step :: Double -> Game -> Game
step time g = 
    if _status g /= Running then g 
    else updatePongBall time
            $ updatePatrick time
            $ updatePongPaddle time
            $ updateBob time
            $ updateInvaders time 
            $ updateBullets time
            $ updateCollisions g

