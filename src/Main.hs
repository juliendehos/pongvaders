{-# LANGUAGE OverloadedStrings #-}

import qualified Item as G
import qualified Game as G
import qualified Constants as C

import qualified Data.Set as S
import qualified JavaScript.Web.Canvas as JSC
import qualified Miso.String as MS

import Control.Monad (when)
import Data.Map (singleton)
import Miso
import System.Random (newStdGen, randoms)

----------------------------------------------------------------------
-- types & params
----------------------------------------------------------------------

data Model = Model
    { _game :: G.Game
    , _time :: Double
    , _rands :: [Double]
    } deriving (Eq)

data Action
    = ActionNone
    | ActionReset
    | ActionDisplay Bool
    | ActionStep Double
    | ActionKey (S.Set Int)

----------------------------------------------------------------------
-- main functions
----------------------------------------------------------------------

myGetTime :: IO Double
myGetTime = (* 0.001) <$> now

main :: IO ()
main = do
    bobImg <- jsNewImage
    patImg <- jsNewImage
    jsSetSrc bobImg C.bobImgName
    jsSetSrc patImg C.patImgName
    rands <- take 1000 . randoms <$> newStdGen
    time0 <- myGetTime
    let game0 = G.createGame False rands 
    startApp App
        { initialAction = ActionDisplay False
        , update        = updateModel bobImg patImg
        , view          = viewModel
        , model         = Model game0 time0 rands
        , subs          = [ keyboardSub ActionKey ]
        , events        = defaultEvents
        , mountPoint    = Nothing
        }

----------------------------------------------------------------------
-- view function
----------------------------------------------------------------------

viewModel :: Model -> View Action
viewModel _ = div_ []
    [ h1_ [] [ text "pongvaders" ]
    , p_ [] [ canvas_ [ id_ "mycanvas"
                      , width_ (MS.ms C.gameWidth)
                      , height_ (MS.ms C.gameHeight)
                      , style_  (singleton "border" "1px solid black")
                      ] []
            ]
    , p_ [] [ audio_ [ id_ "myaudio", src_ "laugh.mp3" ] [] ]
    , p_ [] 
         [ "Usage: up/down to move Patrick, left/right/space to move/fire Bob, enter to start... " 
         , a_ [ href_ "https://gitlab.com/juliendehos/pongvaders"]
              [ text "source code" ]
         ]
    ]

drawItem :: JSC.Context -> G.Item -> IO ()
drawItem ctx (G.Item (sx, sy) (px, py) _) = 
    JSC.fillRect (px-0.5*sx) (py-0.5*sy) sx sy ctx

drawItemBall :: JSC.Context -> G.Item -> IO ()
drawItemBall ctx (G.Item (sx, _) (px, py) _) = do
    JSC.beginPath ctx
    JSC.strokeStyle 0 0 0 0 ctx
    JSC.arc px py sx 0 (2*pi) False ctx
    JSC.fill ctx

drawGame :: JSC.Image -> JSC.Image -> G.Game -> IO ()
drawGame bobImg patImg game = do
    -- initialize drawing
    ctx <- jsGetCtx
    JSC.clearRect 0 0 C.gameWidthD C.gameHeightD ctx
    JSC.lineWidth 0 ctx
    -- draw bullets
    JSC.fillStyle 0 0 255 255 ctx
    mapM_ (drawItem ctx) (G._bullets game)
    -- draw invaders
    JSC.fillStyle 255 0 0 255 ctx
    mapM_ (drawItem ctx) (G._invaders game)
    JSC.stroke ctx
    -- draw bob
    jsDrawImage bobImg (xBob-0.5*C.bobWidthD) (yBob-0.5*C.bobHeightD) ctx
    -- draw pong (patrick, paddle, ball)
    jsDrawImage patImg (xPat-0.5*C.patWidthD) (yPat-0.5*C.patHeightD) ctx
    JSC.fillStyle 0 120 0 255 ctx
    drawItem ctx (G._pongPaddle game)
    drawItemBall ctx (G._pongBall game)
    where (xBob, yBob) = G._pos $ G._bob game
          (xPat, yPat) = G._pos $ G._patrick game

drawText :: MS.JSString -> IO ()
drawText txt = do
    ctx <- jsGetCtx
    JSC.clearRect 0 0 C.gameWidthD C.gameHeightD ctx
    JSC.fillStyle 0 0 0 255 ctx
    JSC.textAlign JSC.Center ctx
    JSC.font "40px Arial" ctx
    JSC.fillText txt (0.5*C.gameWidthD) (0.5*C.gameHeightD) ctx
    JSC.stroke ctx

----------------------------------------------------------------------
-- update function
----------------------------------------------------------------------

updateModel :: JSC.Image -> JSC.Image -> Action -> Model -> Effect Action Model

updateModel _ _ ActionNone m = noEff m

updateModel _ _ ActionReset m = m' <# pure (ActionDisplay False)
    where rands' = G.doCycle 1 (_rands m)
          game' = G.createGame True rands' 
          m' = m { _game = game', _rands = rands' }

updateModel _ _ (ActionDisplay hasShoot) m = m <# case G._status (_game m) of
    G.Welcome -> drawText "Welcome ! Press Enter to start..." >> pure ActionNone
    G.Won -> drawText "You win !" >> pure ActionNone
    G.Lost -> drawText "Game over !" >> pure ActionNone
    _ -> when hasShoot jsPlayAudio >> ActionStep <$> myGetTime

updateModel bobImg patImg (ActionStep t1) m = m' <# do
    drawGame bobImg patImg game
    pure (ActionDisplay hasShoot)
    where t0 = _time m
          dt = t1 - t0
          game = _game m
          game' = G.step dt game
          hasShoot = length (G._invaders game') < length (G._invaders game)
          m' = m { _game = game', _time = t1 }

updateModel _ _ (ActionKey ks) m = 
    if S.member 13 ks 
    then m <# (jsConsoleLog "new game" >> pure ActionReset)
    else noEff m { _game = (_game m) { G._inputUp = S.member 38 ks
                                     , G._inputDown = S.member 40 ks
                                     , G._inputLeft = S.member 37 ks
                                     , G._inputRight = S.member 39 ks
                                     , G._inputFire = S.member 32 ks
                                     }
                 }

----------------------------------------------------------------------
-- JavaScript FFI
----------------------------------------------------------------------

foreign import javascript unsafe "$r = new Image();"
    jsNewImage :: IO JSC.Image

foreign import javascript unsafe "$1.src = $2;"
    jsSetSrc :: JSC.Image -> MS.MisoString -> IO ()

foreign import javascript unsafe "$r = mycanvas.getContext('2d');"
    jsGetCtx :: IO JSC.Context

foreign import javascript unsafe "$4.drawImage($1, $2, $3);"
    jsDrawImage :: JSC.Image -> Double -> Double -> JSC.Context -> IO ()

foreign import javascript unsafe "console.log($1);"
    jsConsoleLog :: MS.MisoString -> IO ()

foreign import javascript unsafe "myaudio.play();"
    jsPlayAudio :: IO ()

