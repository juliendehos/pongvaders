module Bounce where

import Item
import qualified Point as P

lerp :: Double -> Double -> Double -> Double -> Double -> Double
lerp a0 b0 a1 b1 b = if abs b1b0 < 1e-3 then a0 else a0 + a1a0*bb0/b1b0
    where a1a0 = a1 - a0
          b1b0 = b1 - b0
          bb0 = b - b0

bounceItem :: Item -> Item -> Item -> (Bool, Item)
bounceItem ball0 ball1 item = (bounce, ball0 { _pos = (px, py), _vel = (vx, vy) })
    where (Item is0 ip0 _) = item
          (ipx0, ipy0) = ip0 P.- 0.5 P.* is0
          (ipx1, ipy1) = ip0 P.+ 0.5 P.* is0
          (Item _ (bpx0, bpy0) (bvx0, bvy0)) = ball0
          (Item _ (bpx1, bpy1) (bvx1, bvy1)) = ball1
          xx0 = lerp bpx0 bpy0 bpx1 bpy1 ipy0
          xx1 = lerp bpx0 bpy0 bpx1 bpy1 ipy1
          yy0 = lerp bpy0 bpx0 bpy1 bpx1 ipx0
          yy1 = lerp bpy0 bpx0 bpy1 bpx1 ipx1
          (bounce, px, py, vx, vy)
                | bpx1<ipx1 && ipx1<bpx0 && ipy0<yy1 && yy1<ipy1 = (True, 2*ipx1-bpx1, bpy1, -bvx0, bvy0)
                | bpx0<ipx0 && ipx0<bpx1 && ipy0<yy0 && yy0<ipy1 = (True, 2*ipx0-bpx1, bpy1, -bvx0, bvy0)
                | bpy0<ipy0 && ipy0<bpy1 && ipx0<xx0 && xx0<ipx1 = (True, bpx1, 2*ipy0-bpy1, bvx0, -bvy0)
                | bpy1<ipy1 && ipy1<bpy0 && ipx0<xx1 && xx1<ipx1 = (True, bpx1, 2*ipy1-bpy1, bvx0, -bvy0)
                | otherwise = (False, bpx1, bpy1, bvx1, bvy1)

bounceItems :: Item -> Item -> [Item] -> Item
bounceItems _ ball1 [] = ball1
bounceItems ball0 ball1 (x:xs) = if bounce then ball else bounceItems ball0 ball1 xs
    where (bounce, ball) = bounceItem ball0 ball1 x

