module Item where

import qualified Point as P

data Item = Item
    { _siz :: P.Point Double
    , _pos :: P.Point Double
    , _vel :: P.Point Double
    } deriving (Eq)

