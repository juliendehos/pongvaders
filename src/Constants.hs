{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Constants where

import qualified Miso.String as MS

gameWidthInvaders = 400 :: Int
gameHeightInvaders = 600 :: Int
gameWidthD = fromIntegral gameWidth :: Double
gameHeightD = fromIntegral gameHeight :: Double

gameXmargin = 300 :: Int
gameYmargin = 50 :: Int
gameXmarginD = fromIntegral gameXmargin :: Double
gameYmarginD = fromIntegral gameYmargin :: Double

gameWidth = gameWidthInvaders + 2*gameXmargin :: Int
gameHeight = gameHeightInvaders + 2*gameYmargin :: Int
gameWidthInvadersD = fromIntegral gameWidthInvaders :: Double
gameHeightInvadersD = fromIntegral gameHeightInvaders :: Double

bobWidthD = 70 :: Double
bobHeightD = 66 :: Double
bobImgName = "spongebob-small.png" :: MS.MisoString

patWidthD = 60 :: Double
patHeightD = 80 :: Double
patImgName = "patrick-small.png" :: MS.MisoString

pongPaddleWD = 20 :: Double
pongPaddleHD = 70 :: Double

pongBallD = 10 :: Double

timeCanFire = 3 :: Double

